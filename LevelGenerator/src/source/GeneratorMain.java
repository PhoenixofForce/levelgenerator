package source;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;
import java.util.Stack;
import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JTextField;

public class GeneratorMain extends JFrame implements Runnable{
	private static final long serialVersionUID = 1L;
	public Image Icon;{
		try {
			Icon = ImageIO.read(new File("LevelGenerato.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
		
	//TEEESt
	JFrame RoomFrame = new JFrame();
	JFrame EraseFrame = new JFrame();
	JFrame OptionFrame = new JFrame();
	//JFrame RoomFrame = new JFrame();
		
	//Strings
	String Version = "v 1.4";
	
	//Varibeln
	int DotSize = 4;
	int Widht = 101;
	int Height = 101;
	int NumberOfTools = 6; 
	int LoopRoom=1;
	int LoopErase=1;
	int Rooms = 300;
	int MouseX;
	int MouseY;
	//Editable
	int ChestPercentRoom=100;
	int ChestPercentFloor=10;
	int RoomSize=40;
	int DoorsPerRoom=5;
	
	//Arrays
	String[] Size = {"2","4","8","16","32","64"};
	String[] Option = {"Ueberlappend", "Schneiden"};
	int Map[][];
	Color[] colors = new Color[512];

	//Booleans
	boolean isMapDrawable=false;
	boolean drawBorder =false;
	boolean generatingRooms=true;
	
	//ComboBoxen
	JComboBox<String> ChooseSize = new JComboBox<String>(Size);
	JComboBox<String> ChangeOption = new JComboBox<String>(Option);
	
	//Textfelder
	JTextField InsertWidht = new JTextField("Insert Width");
	JTextField InsertHeight = new JTextField("Insert Height");
	JTextField InsertLoopRoom = new JTextField("How often");
	JTextField InsertLoopErase = new JTextField("How often");
	
	JTextField InsertDoorsPerRoom = new JTextField("How many Doors per Room");
	JTextField InsertRoomSize = new JTextField("Max bigness of Rooms");
	JTextField InsertChestRoom = new JTextField("Every ... field in a room is a chest");
	JTextField InsertChestFloor = new JTextField("Every ... impasse is a chest");
	
	//JButtons
	JButton GenerateRooms = new JButton("Generate Room");
	JButton GenerateFloors = new JButton("Generate Corridor");
	JButton GenerateDoors = new JButton("Generate Door");
	JButton EraseImpasse = new JButton("Erase Impasse");
	JButton GenerateChests = new JButton("Generate Chest");
	JButton GenerateMap = new JButton("Generate Map");
	JButton Save = new JButton("Save");
	JButton Settings = new JButton("Settings");
	JButton GoGenerating = new JButton("Generate Rooms");
	JButton GoErasing = new JButton("Erase");
	JButton Cancel = new JButton("Cancel Generating");
	
	public GeneratorMain() throws IOException{
		super("LevelGenerator by PhoenixofForce");		//Titel des Programms
		setDefaultCloseOperation(EXIT_ON_CLOSE);	//Beim Schlie�en => Programm beenden
		this.setLayout(null);	//?
		this.setIconImage(Icon);
		setVisible(true);
		final Insets i = getInsets();
		
		File f = new File("C:\\Programme\\LevelGenerator\\Options\\" +"Options.txt");
		try {
			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);
			RoomSize = Integer.parseInt(br.readLine());
			ChestPercentFloor = Integer.parseInt(br.readLine());
			ChestPercentRoom = Integer.parseInt(br.readLine());
			DoorsPerRoom = Integer.parseInt(br.readLine());
			br.close();
			
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		ArrayList<Color> colours = new ArrayList<Color>();
		for(int r = 0; r<8; r++){
			for(int g = 0; g<8; g++){
				for(int b = 0; b<8; b++){
					colours.add(new Color(r*32, g*32, b*32));
				}
			}
		}
		
		Random j = new Random();
		for(int i2 = 0; i2<512; i2++){
			int v = j.nextInt(colours.size());
			colors[i2] = colours.get(v);
			colours.remove(v);
		}
		
		
		//TextFelder
		add(InsertWidht);
		InsertWidht.setBounds((DotSize * Widht) + i.left, 0, 150, (DotSize * Height)/NumberOfTools);
		
		add(InsertHeight);
		InsertHeight.setBounds((DotSize * Widht) + i.left, (1 * (DotSize * Height)/NumberOfTools), 150, (DotSize * Height)/NumberOfTools);
		
		//ComboBoxen
		add(ChooseSize);
		ChooseSize.setSelectedIndex(1);
		ChooseSize.setBounds((DotSize * Widht) + i.left, (2 * (DotSize * Height)/NumberOfTools), 150, (DotSize * Height)/NumberOfTools);
		
		//JButtons
		add(GenerateChests);
		
		add(GenerateDoors);
		
		add(GenerateFloors);
		
		add(GenerateRooms);
		
		add(EraseImpasse);
		
		add(Save);
		Save.setBounds((DotSize * Widht) + i.left, (5 * (DotSize * Height)/NumberOfTools), 150, (DotSize * Height)/NumberOfTools);

		
		add(Settings);
		Settings.setBounds((DotSize * Widht) + i.left, (4 * (DotSize * Height)/NumberOfTools), 150, (DotSize * Height)/NumberOfTools);
		
		add(GenerateMap);
		GenerateMap.setBounds((DotSize * Widht) + i.left, (3 * (DotSize * Height)/NumberOfTools), 150, (DotSize * Height)/NumberOfTools);
		
		//ActionListener
		
		//TextFelder
		InsertWidht.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				int Width2 = Integer.parseInt(InsertWidht.getText());
				if(Width2>=10){
					Widht = Width2;
					updateBounds();
				}
			}
		});
		
		InsertHeight.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				int Height2 = Integer.parseInt(InsertHeight.getText());
				if(Height2>=10){
					Height = Height2;
					updateBounds();
				}
			}
		});
		
		InsertLoopRoom.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				LoopRoom = Integer.parseInt(InsertLoopRoom.getText());
			}
        	
        });
		
		InsertLoopErase.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				LoopErase = Integer.parseInt(InsertLoopErase.getText());
			}
        	
        });
		
		//ComboBoxen
		ChooseSize.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				DotSize = Integer.parseInt((String) ChooseSize.getSelectedItem());
				updateBounds();
			}
		});
		
		ChangeOption.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				if(ChangeOption.getSelectedItem()=="Ueberlappend")drawBorder=false;
				if(ChangeOption.getSelectedItem()=="Schneiden")drawBorder=true;
			}
		});
		
		InsertChestFloor.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				ChestPercentFloor = Integer.parseInt(InsertChestFloor.getText());
			}
        	
        });
		
		InsertChestRoom.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				ChestPercentRoom = Integer.parseInt(InsertChestRoom.getText());
			}
        	
        });
		
		InsertDoorsPerRoom.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				DoorsPerRoom = Integer.parseInt(InsertDoorsPerRoom.getText());
			}
        });
		
		InsertRoomSize.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				RoomSize = Integer.parseInt(InsertRoomSize.getText());
			}
        	
        });
		
		//JButton
		GenerateMap.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				//alte entfernen
				InsertWidht.setBounds(0, 0, 0, 0);
				InsertHeight.setBounds(0, 0, 0, 0);
				ChooseSize.setBounds(0, 0, 0, 0);	
				GenerateMap.setBounds(0, 0, 0, 0);
				
				Insets i = getInsets();
				NumberOfTools = 7;
				GenerateRooms.setBounds((DotSize * Widht) + i.left, 0, 150, (DotSize * Height)/NumberOfTools);
				GenerateFloors.setBounds((DotSize * Widht) + i.left, (1 * (DotSize * Height)/NumberOfTools), 150, (DotSize * Height)/NumberOfTools);
				GenerateChests.setBounds((DotSize * Widht) + i.left, (3 * (DotSize * Height)/NumberOfTools), 150, (DotSize * Height)/NumberOfTools);
				GenerateDoors.setBounds((DotSize * Widht) + i.left, (2 * (DotSize * Height)/NumberOfTools), 150, (DotSize * Height)/NumberOfTools);
				EraseImpasse.setBounds((DotSize * Widht) + i.left, (4 * (DotSize * Height)/NumberOfTools), 150, (DotSize * Height)/NumberOfTools);
				Settings.setBounds((DotSize * Widht) + i.left, (5 * (DotSize * Height)/NumberOfTools), 150, (DotSize * Height)/NumberOfTools);
				Save.setBounds((DotSize * Widht) + i.left, (6 * (DotSize * Height)/NumberOfTools), 150, (DotSize * Height)/NumberOfTools);

				
				Map = new int[Widht][Height];
				for(int x = 1; x < Widht-1; x+=2){
					for(int y = 1; y < Height-1; y+=2){
						Map[x][y] = 1;
					}
				}
				
				isMapDrawable = true;
				drawMap();
			}
		});
		
		GenerateRooms.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
		        RoomFrame.setVisible(true);
		        RoomFrame.setSize(325, 190);
		        RoomFrame.setLayout(null);
		        
		        RoomFrame.add(ChangeOption);
		        ChangeOption.setBounds(0,75,300,75);
		        
		        RoomFrame.add(GoGenerating);
		        GoGenerating.setBounds(175,0,150,75);
		        
		        RoomFrame.add(InsertLoopRoom);
		        InsertLoopRoom.setBounds(0,0,150,75);
		        
		        
		        
		        
				/*boolean isFinished = false;
				while(!isFinished){
					Random r = new Random();
					int x1 = r.nextInt((Widht-1)-1)+1;
					int y1 = r.nextInt((Height-1)-1)+1;
				
					int x2 = r.nextInt((Widht-1)-1)+1;
					int y2 = r.nextInt((Height-1)-1)+1;
				
					if(x1 < x2){
						int f = x1;
						x1 = x2;
						x2 = f;
					}
				
					if(y1 < y2){
						int f = y1;
						y1 = y2;
						y2 = f;
					}
					
					if(Math.abs((x1-x2)*(y1-y2))<=684/DotSize && Map[x1][y1]==1 && Map[x2][y2]==1 && (y1-y2)>1 && (x1-x2)>1&&(x1-x2)/(y1-y2)<1.5&&(y1-y2)/(x1-x2)<1.5){
							isFinished=true;
							
							if(drawBorder == true){
								for(int x = x2-1; x<=x1; x++){
									Map[x][y1+1] = 0;
									Map[x][y2-1] = 0;
								}
						
								for(int y = y2-1; y<=y1; y++){
									Map[x1+1][y] = 0;
									Map[x2-1][y] = 0;
								}
							}
							
						for(int x = x2; x <= x1; x++){
							for(int y = y2; y <= y1; y++){
								Map[x][y]=Rooms;
							}
						
						}
						Rooms+=1;
					}else{
						isFinished =false;
					}
				}
				drawMap();
				isFinished=false;*/
			}
		});
		
		GoGenerating.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				int i=0;
				for(int a =0; a<LoopRoom; a++){
						boolean isFinished = false;
						while(!isFinished){
							Random r = new Random();
							int x1 = r.nextInt((Widht-1)-1)+1;
							int y1 = r.nextInt((Height-1)-1)+1;
						
							int x2 = r.nextInt((Widht-1)-1)+1;
							int y2 = r.nextInt((Height-1)-1)+1;
						
							if(x1 < x2){
								int f = x1;
								x1 = x2;
								x2 = f;
							}
						
							if(y1 < y2){
								int f = y1;
								y1 = y2;
								y2 = f;
							}
							
							if(Math.abs((x1-x2)*(y1-y2))<=((Height*Widht)/750)*RoomSize && Map[x1][y1]==1 && Map[x2][y2]==1 && (y1-y2)>1 && (x1-x2)>1&&(x1-x2)/(y1-y2)<1.5&&(y1-y2)/(x1-x2)<1.5){
									isFinished=true;
									i=0;
									if(drawBorder == true){
										for(int x = x2-1; x<=x1; x++){
											Map[x][y1+1] = 0;
											Map[x][y2-1] = 0;
										}
								
										for(int y = y2-1; y<=y1; y++){
											Map[x1+1][y] = 0;
											Map[x2-1][y] = 0;
										}
									}
									
								for(int x = x2; x <= x1; x++){
									for(int y = y2; y <= y1; y++){
										Map[x][y]=Rooms;
									}
								
								}
								Rooms+=1;
							}else{
								i++;
								isFinished =false;
								if(i>=2000000){
									isFinished =true;
									a=LoopRoom;
								}
							}
						}
						drawMap();
						isFinished=false;
				}
				RoomFrame.dispose();
			}
		});
		
		GenerateFloors.addActionListener(new ActionListener(){
			boolean isGenerated=false;
			@Override
			public void actionPerformed(ActionEvent e) {
				boolean isFinished=false;
				while(!isFinished&&!isGenerated){
					for(int x1 = 1; x1<Widht-1; x1++){
						for(int y1 = 1; y1<Height-1; y1++){
							if(Map[x1][y1]==1){
								isFinished=true;
								Stack<int[]> s = new Stack<int[]>();
								int f[] = new int[]{x1,y1};
								s.add(f);
								Map[f[0]][f[1]]=3;
								while(!s.isEmpty()){
									/*
									try {
										Thread.sleep(1);
									} catch (InterruptedException e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									}*/
									int a[] = s.peek();
									boolean p1=false;
									boolean p2=false;
									boolean p3=false;
									boolean p4=false;
									
									if(a[0]>2&&Map[a[0]-2][a[1]]==1)p1=true;
									if(a[0] < Widht - 2 && Map[a[0]+2][a[1]]==1)p2=true;	
									if(a[1] > 2 && Map[a[0]][a[1]-2] == 1)p3=true;
									if(a[1] < Height-2 && Map[a[0]][(a[1]+2)] == 1)p4=true;
									
									if(p1 || p2 || p3 || p4){
										boolean finished= false;
										while(!finished){
											Random r = new Random();
											int w = r.nextInt(4)+1;
											
											if(p1 && w==1){
												Map[a[0]-2][a[1]]=3;
												Map[a[0]-1][a[1]]=3;
												int wi[] = new int[]{a[0]-2,a[1]};
												s.add(wi);
												finished=true;
											}
											
											if(p2 && w==2){
												Map[a[0]+2][a[1]]=3;
												Map[a[0]+1][a[1]]=3;
												int wi[] = new int[]{a[0]+2,a[1]};
												s.add(wi);
												finished=true;
											}
											
											if(p3 && w==3){
												Map[a[0]][a[1]-2]=3;
												Map[a[0]][a[1]-1]=3;
												int wi[] = new int[]{a[0],a[1]-2};
												s.add(wi);
												finished=true;
											}
											
											if(p4 && w==4){
												Map[a[0]][a[1]+2]=3;
												Map[a[0]][a[1]+1]=3;
												int wi[] = new int[]{a[0],a[1]+2};
												s.add(wi);
												finished=true;
											}
											isGenerated=true;
										}
										//drawMap();
									}else{
										s.pop();
									}
									
									
								}
								drawMap();
							}
						}
					}
				}
			}
		});
		
		GenerateDoors.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				ArrayList<ArrayList<int[]>> RoomList= new ArrayList<ArrayList<int[]>>();
				for(int r = 0; r<(Rooms-300); r++){
					RoomList.add(new ArrayList<int[]>());
				}
				
				
				for(int x = 1; x<Widht-1; x++){
					for(int y = 1; y<Height-1; y++){
						if(Map[x][y]==0){
							if(Map[x][y+1]!=3 && (Map[x][y-1]!=Map[x][y+1]&&(Map[x][y-1]!=0&&Map[x][y-1]!=1))){
								for(int f=300; f<Rooms; f++){
									if(Map[x][y+1]==f){
										RoomList.get(f-300).add(new int[]{x,y});
									}
								}
								
							}if(Map[x][y-1]!=3 && Map[x][y+1]==3){
								for(int f=300; f<Rooms; f++){
									if(Map[x][y-1]==f){
										RoomList.get(f-300).add(new int[]{x,y});
									}
								}
							}if(Map[x+1][y]!=3&& Map[x-1][y]==3){
								for(int f=300; f<Rooms; f++){
									if(Map[x+1][y]==f){
										RoomList.get(f-300).add(new int[]{x,y});
									}
								}
								
							}if(Map[x-1][y]!=3 && Map[x+1][y]==3){
								for(int f=300; f<Rooms; f++){
									if(Map[x-1][y]==f){
										RoomList.get(f-300).add(new int[]{x,y});
									}
								}
							}
						}
					}
				}
				
				for(int i2 =0; i2<RoomList.size();i2++){
					ArrayList<int[]> Room = RoomList.get(i2); 
					if(Room.size()>=DoorsPerRoom){
						for(int o=0; o<DoorsPerRoom; o++){
							Random r = new Random();
							int[] Pos = Room.get(r.nextInt(Room.size()));
							Room.remove(Pos);
							Map[Pos[0]][Pos[1]]=3;	
						}
					} else for(int o=0; o<Room.size(); o++){
						Random r = new Random();
						int[] Pos = Room.get(r.nextInt(Room.size()));
						Room.remove(Pos);
						Map[Pos[0]][Pos[1]]=3;
					}
					for(int x=0; x<Widht; x++){
						for(int y=0; y<Height; y++){
							if(Map[x][y]==i2+300){
								Map[x][y]=3;
							}
						}
					}
				}
				drawMap();
			}
		});
		
		EraseImpasse.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				EraseFrame.setVisible(true);
		        EraseFrame.setSize(325, 115);
		        EraseFrame.setLayout(null);
		        
		        EraseFrame.add(GoErasing);
		        GoErasing.setBounds(175,0,150,75);
		        
		        EraseFrame.add(InsertLoopErase);
		        InsertLoopErase.setBounds(0,0,150,75);
				
				/*int Count=0;
				for(int x = 1; x<Widht-1; x++){
					for(int y = 1; y<Height-1; y++){
						if(Map[x][y]==3){	
							if(Map[x+1][y]==3||Map[x+1][y]==2)Count+=1;
							if(Map[x-1][y]==3||Map[x-1][y]==2)Count+=1;	
							if(Map[x][y+1]==3||Map[x][y+1]==2)Count+=1;
							if(Map[x][y-1]==3||Map[x][y-1]==2)Count+=1;
							if(Count==1)Map[x][y]=0;
							Count = 0;
						}
					}
				}
				drawMap();*/
			}
		});
		
		GoErasing.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				if(LoopErase<50){
						for(int a=0; a<LoopErase; a++){
							int c =1;
							if(c>0){
								c=0;
								int Count=0;
								for(int x = 1; x<Widht; x++){
									for(int y = 1; y<Height; y++){
										if(Map[x][y]==3){	
											if(Map[x+1][y]==3||Map[x+1][y]==2)Count++;
											if(Map[x-1][y]==3||Map[x-1][y]==2)Count++;	
											if(Map[x][y+1]==3||Map[x][y+1]==2)Count++;
											if(Map[x][y-1]==3||Map[x][y-1]==2)Count++;
											if(Count<=1)Map[x][y]=0;
											Count = 0;
											c++;
										}
									}
								}
								drawMap();
							}else{
								a=LoopErase;
							}
						}
				}else{
					int c=0;
					do{
						c=0;
						int Count=0;
						for(int x = 1; x<Widht-1; x++){
							for(int y = 1; y<Height-1; y++){
								if(Map[x][y]==3){	
									if(Map[x+1][y]==3||Map[x+1][y]==2)Count+=1;
									if(Map[x-1][y]==3||Map[x-1][y]==2)Count+=1;	
									if(Map[x][y+1]==3||Map[x][y+1]==2)Count+=1;
									if(Map[x][y-1]==3||Map[x][y-1]==2)Count+=1;
									if(Count<=1){
										Map[x][y]=0;
										c++;
									}
									Count = 0;
								}
							}
						}
						drawMap();
					}while(c>0);
				}
				EraseFrame.dispose();
			}
		});
		
		
		GenerateChests.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				for(int x = 1; x<Widht-1; x++){
					for(int y = 1; y<Height-1; y++){
						if(Map[x][y]==3){	
							int Count=0;
							if((Map[x][y-1]==3)&&(Map[x][y+1]==3)&&Map[x-1][y]==3&&(Map[x+1][y]==3)&&(Map[x+1][y+1]==3)&&(Map[x+1][y-1]==3)&&(Map[x-1][y-1]==3)&&(Map[x-1][y+1]==3)){
								Count+=1;
								Random r = new Random();
								if(Count==1&& r.nextInt(ChestPercentRoom)==0)Map[x][y]=2;
							}
							else{
								if(Map[x+1][y]==3||Map[x+1][y]==2)Count+=1;
								if(Map[x-1][y]==3||Map[x-1][y]==2)Count+=1;	
								if(Map[x][y+1]==3||Map[x][y+1]==2)Count+=1;
								if(Map[x][y-1]==3||Map[x][y-1]==2)Count+=1;
								Random r = new Random();
								if(Count==1&& r.nextInt(ChestPercentFloor)==0)Map[x][y]=2;
							}
						}
					}
				}
				drawMap();
			}
		});
		
		Cancel.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				generatingRooms=false;
			}
			
		});
		
		Save.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				saveMap();
				saveOptions();
			}
		});
		
		Settings.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				OptionFrame.setVisible(true);
		        OptionFrame.setSize(365, 190);
		        OptionFrame.setLayout(null);
		        
		        OptionFrame.add(InsertDoorsPerRoom);
		        OptionFrame.add(InsertRoomSize);
		        OptionFrame.add(InsertChestRoom);
		        OptionFrame.add(InsertChestFloor);
		        
		        InsertChestFloor.setBounds(0,0,175,75);
		        InsertChestRoom.setBounds(0,75,175,75);
		        InsertRoomSize.setBounds(175,0,175,75);
		        InsertDoorsPerRoom.setBounds(175,75,175,75);
			}
		});
		
		addMouseListener(new MouseAdapter() {
			  public void mousePressed(MouseEvent e) {
				  MouseX=e.getX() - i.left;
				  MouseY=e.getY() - i.top;
				  System.out.println(MouseX + "  " + MouseY);
				  
				  int MapX = MouseX / DotSize;
				  int MapY = MouseY / DotSize;
				  System.out.println(MapX + "  1  " + MapY);

				  
				  if(isMapDrawable){
					  if(Map[MapX][MapY]==0)Map[MapX][MapY]=3;
					  else if(Map[MapX][MapY]==3)Map[MapX][MapY]=2;
					  else if(Map[MapX][MapY]==2)Map[MapX][MapY]=0;
					  //System.out.println(MapX + "  2  " + MapY);
				  }
				  drawMap();
	            }
	 
	 
		});
		
		
		
		
		
		
		
		
		
		
		
		setSize(150 + (DotSize * Widht) + i.left + i.right, (DotSize * Height) + i.bottom + i.top);
	}
	
	public void updateBounds(){
		Insets i = getInsets();
		setSize(150 + (DotSize * Widht) + i.left + i.right, (DotSize * Height) + i.bottom + i.top);
		InsertWidht.setBounds((DotSize * Widht) + i.left, 0, 150, (DotSize * Height)/NumberOfTools);
		InsertHeight.setBounds((DotSize * Widht) + i.left, (1 * (DotSize * Height)/NumberOfTools), 150, (DotSize * Height)/NumberOfTools);
		ChooseSize.setBounds((DotSize * Widht) + i.left, (2 * (DotSize * Height)/NumberOfTools), 150, (DotSize * Height)/NumberOfTools);
		GenerateMap.setBounds((DotSize * Widht) + i.left, (3 * (DotSize * Height)/NumberOfTools), 150, (DotSize * Height)/NumberOfTools);
		Settings.setBounds((DotSize * Widht) + i.left, (4 * (DotSize * Height)/NumberOfTools), 150, (DotSize * Height)/NumberOfTools);
		Save.setBounds((DotSize * Widht) + i.left, (5 * (DotSize * Height)/NumberOfTools), 150, (DotSize * Height)/NumberOfTools);
	}
	
	public void drawMap(){
		if(isMapDrawable){
			Insets i = getInsets();
			Graphics g = getGraphics();
			for(int x = 0; x < Widht; x++){
				for(int y = 0; y < Height; y++){
					if(Map[x][y]==0){
						g.setColor(Color.BLACK);
						g.fillRect(x * DotSize + i.left, y * DotSize + i.top, DotSize, DotSize);
					} else if(Map[x][y]==1){
						g.setColor(Color.CYAN);
						g.fillRect(x * DotSize + i.left, y * DotSize + i.top, DotSize, DotSize);
					}else if(Map[x][y]==2){
						g.setColor(Color.YELLOW);
						g.fillRect(x * DotSize + i.left, y * DotSize + i.top, DotSize, DotSize);
					}else if(Map[x][y]==3){
						g.setColor(Color.RED);
						g.fillRect(x * DotSize + i.left, y * DotSize + i.top, DotSize, DotSize);
					}
					for(int f=300 ; f <= 812; f++){
						if(Map[x][y]==f){
							g.setColor(colors[f-300]);
							g.fillRect(x * DotSize + i.left, y * DotSize + i.top, DotSize, DotSize);
						}
						
						
					}
					
				}
			}
		}
	}
	
	public void saveMap(){
		if(isMapDrawable){
			File f;
			FileWriter fw;	
			String date = new SimpleDateFormat("MM.dd.yyyy").format(new Date());
			String time = new SimpleDateFormat("HH_mm_ss").format(new Date());
			f = new File("C:\\Programme\\LevelGenerator\\Maps\\"  );
			f.mkdirs();
			f = new File("C:\\Programme\\LevelGenerator\\Maps\\" + date +" " + time +".txt");
			
			try {
				fw = new FileWriter(f, true);
				
				for(int y=1; y<Height;y++){
					for(int x=1; x<Widht;x++){
						fw.write(Map[x][y] + " " );
					}
					fw.write(System.getProperty("line.separator"));
				}
				
				fw.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void saveOptions(){
		File f;
		FileWriter fw;	
		f = new File("C:\\Programme\\LevelGenerator\\Options\\");
		f.mkdirs();
		f = new File("C:\\Programme\\LevelGenerator\\Options\\" +"Options.txt");
		if(f.exists())f.delete();
		try {
			fw = new FileWriter(f, true);
			
			
			fw.write(RoomSize + "\n");
			//fw.write(System.getProperty("line.separator"));
	
			fw.write(ChestPercentFloor + "\n");
			//fw.write(System.getProperty("line.separator"));
			
			fw.write(ChestPercentRoom + "\n");
			//fw.write(System.getProperty("line.separator"));
			
			fw.write(DoorsPerRoom + "\n");			
			fw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public static void main(String[] args) {
		try {
			new GeneratorMain();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void run() {
		
	}
}
